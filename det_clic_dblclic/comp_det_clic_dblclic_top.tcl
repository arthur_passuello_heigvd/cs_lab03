###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : comp_det_clic_dblclic_top.tcl
# Description  : Script de compilation des fichiers
# 
# Auteur       : Etienne Messerli, le 29.11.2017
#
# Utilise      : Compilation projet Det_Clic_Dbl_Clic
#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date   Description
#                         
############################################################################


#create library work        
vlib work
#map library work to work
vmap work work

# Files compilation
# to be complete with all your files
#vcom -reportprogress 300 -work work   ../src/ **** to be complete *****

vcom -reportprogress 300 -work work ../src/timer.vhd
vcom -reportprogress 300 -work work ../src/maintien.vhd
vcom -reportprogress 300 -work work ../src/mss_clic_dbclic.vhd

#compile top file
vcom -reportprogress 300 -work work ../src/det_clic_dblclic_top_optimise.vhd

#compile fichier tb et tester 
vcom -reportprogress 300 -work work ../src_tb/det_clic_dblclic_top_tester.vhd
vcom -reportprogress 300 -work work ../src_tb/det_clic_dblclic_top_tb.vhd

