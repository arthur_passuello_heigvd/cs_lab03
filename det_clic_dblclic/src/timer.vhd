-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : timer.vhd
-- Auteur       : Etienne Messerli
-- 
-- Description  : Detection d'un clic et double clic
-- 
-- Utilise      : Labo CSN 2017
--| Modifications |------------------------------------------------------------
-- Ver   Date      Qui         Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity timer is
    port (
        clock_i    : in  std_logic;
        reset_i    : in  std_logic;
        start_i    : in  std_logic;
        top_ms_i   : in  std_logic;
        trigger1_o : out std_logic;
        trigger2_o : out std_logic
        );
end timer;

architecture comport of timer is

	signal cpt_pres_s, cpt_fut_s, cpt_past_s : unsigned(8 downto 0);
	signal det_zero_s		: std_logic;
	signal det_100_s 		: std_logic;
	constant VAL                 	: natural := 301; --9 bits, delai max 300
begin
	cpt_fut_s <= to_unsigned(VAL, 9)  when start_i  = '1' else
		     	 cpt_pres_s 	when det_zero_s = '1'  or cpt_pres_s = "00000000"  else
		     	 cpt_pres_s - 1      when top_ms_i = '1' else 
		     	 cpt_pres_s          when top_ms_i = '0'else
                 (others => '0');

	process(clock_i, reset_i)
	begin
		if(reset_i = '1') then
			cpt_pres_s <= (others => '0');
		elsif Rising_Edge(clock_i) then
			cpt_past_s <= cpt_pres_s;
			cpt_pres_s <= cpt_fut_s;
		end if;
	end process;
	
	trigger1_o <= '1' when det_100_s = '1' else '0';
	trigger2_o <= '1' when det_zero_s = '1' else '0';
	det_zero_s <= '1' when cpt_pres_s = x"000" and cpt_past_s = x"001" else '0';
	det_100_s <= '1' when cpt_pres_s = x"064" else '0';
	

end comport;
