-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Maintien.vhd
-- Auteur       : Etienne Messerli, le 05.05.2016
-- 
-- Description  : Detection d'un clic et double clic
--                Projet repris du labo Det_Clic_DblClic 2012
-- 
-- Utilise      : Labo SysLog2 2016
--| Modifications |------------------------------------------------------------
-- Ver   Date      Qui         Description
-- 2.0  27.11.2017 EMI    Descritpion sans machine d'etats
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity maintien is
    port (clock_i    : in  std_logic;
          reset_i    : in  std_logic;
          pulse_i    : in  std_logic;
          top_ms_i   : in  std_logic;
          demi_sec_o : out std_logic
          );
end maintien;


architecture comport of maintien is
    type state_type is (Idle, Count);
    constant DEMI_SEC               : natural := 500;
    signal etat_present, etat_futur : state_type;

    --Maintien pendant 0,5 sec => compter 500 p�riodes a 1KHz
    signal cnt_fut, cnt_pres : unsigned(9 downto 0);  --10 bits, delai max 1023
    signal det_zero_s : std_logic;

begin

-- timer --------------------------------------------------
    cnt_fut <= to_unsigned(DEMI_SEC+1, cnt_pres'length)  -- DEMI_SEC+1 compense temps chargement counter
                           when pulse_i = '1'    else    -- init tempo
               cnt_pres    when det_zero_s = '1' else    -- maintien lorsque tempo termine
               cnt_pres -1 when top_ms_i = '1'   else    -- decompte si top_ms
               cnt_pres;                                 -- maintien

    timer : process(clock_i, reset_i)
    begin
        if reset_i = '1' then
            cnt_pres <= (others => '0');
        elsif rising_edge(clock_i) then
            cnt_pres <= cnt_fut;
        end if;
    end process;

    det_zero_s <= '1' when cnt_pres = 0 else
                  '0';
    demi_sec_o <= not det_zero_s;
    
end comport;
