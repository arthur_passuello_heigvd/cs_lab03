-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : det_clic_dblclic_top.vhd
-- Auteur       : Etienne Messerli
-- 
-- Description  : Detection d'un clic et double clic
-- 
-- Utilise      : Labo CSN 2017
--| Modifications |------------------------------------------------------------
-- Ver   Date      Qui         Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity det_clic_dblclic_top is
    port(clock_i       : in  std_logic;  --horloge systeme 1MHz
         nReset_i      : in  std_logic;  --reset asynchrone
         button_i      : in  std_logic;
         top_ms_i      : in  std_logic;
         clic_o        : out std_logic;
         dbl_clic_o    : out std_logic;
         clic_lg_o     : out std_logic;
         dbl_clic_lg_o : out std_logic
         );
end det_clic_dblclic_top;

architecture struct of det_clic_dblclic_top is

   component timer is
    port (
        clock_i    : in  std_logic;
        reset_i    : in  std_logic;
        start_i    : in  std_logic;
        top_ms_i   : in  std_logic;
        trigger1_o : out std_logic;
        trigger2_o : out std_logic
        );
end component;

component maintien is
    port (clock_i    : in  std_logic;
          reset_i    : in  std_logic;
          pulse_i    : in  std_logic;
          top_ms_i   : in  std_logic;
          demi_sec_o : out std_logic
          );
end component;

component mss_clic_dbclic is
    port(clock_i       : in  std_logic;  --horloge systeme 1MHz
         nReset_i      : in  std_logic;  --reset asynchrone
         button_i      : in  std_logic;
         trig_clic     : in  std_logic;
         trig_dbclic   : in  std_logic;
         clic_o        : out std_logic;
         dbl_clic_o    : out std_logic;
         start_timer   : out std_logic
         );
end component;

	signal reset_s : std_logic;
	signal clic_s, dbclic_s : std_logic;
	signal clic_lg_s, dbclic_lg_s : std_logic;
	signal start_timer_s : std_logic;
	signal trig1_s, trig2_s : std_logic;

begin
   reset_s <= not nReset_i;

    TIMER_inst : timer 
	port map (
	clock_i    => clock_i,
        reset_i    => reset_s,
        start_i    => start_timer_s,
        top_ms_i   => top_ms_i,
        trigger1_o => trig1_s,
        trigger2_o => trig2_s
	);

   MSS : mss_clic_dbclic 
	port map (
		clock_i    => clock_i,  --horloge systeme 1MHz
         	nReset_i   => nReset_i,  --reset asynchrone
         	button_i   => button_i,
         	trig_clic  => trig2_s,
         	trig_dbclic=> trig1_s,
         	clic_o     => clic_s,
         	dbl_clic_o => dbclic_s,
         	start_timer=> start_timer_s
	);

   MAINTIENCLIC : maintien 
	port map (
		clock_i    => clock_i,
        	reset_i    => reset_s,
          	pulse_i    => clic_s,
          	top_ms_i   => top_ms_i,
          	demi_sec_o => clic_lg_s
	);

	MAINTIENDCLIC : maintien 
	port map (
		clock_i    => clock_i,
        	reset_i    => reset_s,
          	pulse_i    => dbclic_s,
          	top_ms_i   => top_ms_i,
          	demi_sec_o => dbclic_lg_s
	);

	clic_o 		<= clic_s;
         dbl_clic_o 	<= dbclic_s;
         clic_lg_o  	<= clic_lg_s;
         dbl_clic_lg_o 	<= dbclic_lg_s;

end struct;
