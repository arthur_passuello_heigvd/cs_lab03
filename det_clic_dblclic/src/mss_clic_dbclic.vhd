-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : det_clic_dblclic_top.vhd
-- Auteur       : Etienne Messerli
-- 
-- Description  : Detection d'un clic et double clic
-- 
-- Utilise      : Labo CSN 2017
--| Modifications |------------------------------------------------------------
-- Ver   Date      Qui         Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mss_clic_dbclic is
    port(clock_i       : in  std_logic;  --horloge systeme 1MHz
         reset_i      : in  std_logic;  --reset asynchrone
         button_i      : in  std_logic;
         trig_clic     : in  std_logic;
         trig_dbclic   : in  std_logic;
         clic_o        : out std_logic;
         dbl_clic_o    : out std_logic;
         start_timer   : out std_logic
         );
end mss_clic_dbclic;

architecture struct of mss_clic_dbclic is

	type state_t is (
			START,
			CLIC1_W,
			CLIC2_W,
			CLIC,
			DBCLIC,
			WAIT_C,
			TRIG1,
			TRIG2,
			TRIGW,
			ERR
			);
	
	signal cur_state, next_state : state_t;
	signal trig_timer_s, clic_s, dbclic_s	: std_logic := '0';

begin

    process(cur_state, button_i, trig_clic, trig_dbclic)
    begin
	clic_s <= '0';
	dbclic_s <= '0';
	trig_timer_s <= '0';
	next_state <= START;

       		case cur_state is 
    			when START =>
   					if (button_i = '1') then
    					next_state <= TRIG1;
   					end if;
    			when CLIC1_W =>
    				if (trig_clic = '1') then
   						next_state <= ERR;
   					elsif (button_i = '0') then
   						next_state <= TRIGW;
						else
							next_state <= CLIC1_W;
   					end if;		
    			when CLIC2_W =>
    				if (trig_clic = '1') then
    					next_state <= ERR;
   					elsif (button_i = '0') then
   						next_state <= DBCLIC;
						else
							next_state <= CLIC2_W;
   					end if;
    			when CLIC =>
   					next_state <= START;
					clic_s <= '1';
				when DBCLIC =>
    				next_state <= START;
					dbclic_s <= '1';
    			when WAIT_C =>
    				if (trig_dbclic = '1') then
    					next_state <= CLIC;
    				elsif (button_i = '1') then
    					next_state <= TRIG2;
					else
						next_state <= WAIT_C;
    				end if;
    			when TRIG1 =>
    				next_state <= CLIC1_W;
					trig_timer_s <= '1';
    			when TRIG2 =>
    				next_state <= CLIC2_W;
					trig_timer_s <= '1';
    			when TRIGW =>
    				next_state <= WAIT_C;
					trig_timer_s <= '1';
				when ERR =>
					if(button_i = '0') then
						next_state <= START;
					else
						next_state <= ERR;
					end if;
    			when others =>
    				next_state <= START;
    		end case;	

    end process;  
   
    process(clock_i, reset_i)
    begin
    	if(reset_i = '1') then
    		cur_state 	<= START;
    	elsif (rising_edge(clock_i)) then
            cur_state	<= next_state;  
		end if;
    end process;
   
    clic_o	<= clic_s;
    dbl_clic_o 	<= dbclic_s;
    start_timer <= trig_timer_s;
    
end struct;

