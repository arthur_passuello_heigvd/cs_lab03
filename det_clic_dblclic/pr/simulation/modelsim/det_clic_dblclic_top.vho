-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.0 Build 156 04/24/2013 SJ Full Version"

-- DATE "12/20/2017 10:35:46"

-- 
-- Device: Altera 5M1270ZF256C5 Package FBGA256
-- 

-- 
-- This VHDL file should be used for QuestaSim (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY MAXV;
USE IEEE.STD_LOGIC_1164.ALL;
USE MAXV.MAXV_COMPONENTS.ALL;

ENTITY 	det_clic_dblclic_top IS
    PORT (
	clock_i : IN std_logic;
	nReset_i : IN std_logic;
	button_i : IN std_logic;
	top_ms_i : IN std_logic;
	clic_o : OUT std_logic;
	dbl_clic_o : OUT std_logic;
	clic_lg_o : OUT std_logic;
	dbl_clic_lg_o : OUT std_logic
	);
END det_clic_dblclic_top;

-- Design Ports Information
-- clock_i	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- nReset_i	=>  Location: PIN_J5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- button_i	=>  Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- top_ms_i	=>  Location: PIN_F11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- clic_o	=>  Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- dbl_clic_o	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- clic_lg_o	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
-- dbl_clic_lg_o	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA


ARCHITECTURE structure OF det_clic_dblclic_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clock_i : std_logic;
SIGNAL ww_nReset_i : std_logic;
SIGNAL ww_button_i : std_logic;
SIGNAL ww_top_ms_i : std_logic;
SIGNAL ww_clic_o : std_logic;
SIGNAL ww_dbl_clic_o : std_logic;
SIGNAL ww_clic_lg_o : std_logic;
SIGNAL ww_dbl_clic_lg_o : std_logic;
SIGNAL \clock_i~combout\ : std_logic;
SIGNAL \button_i~combout\ : std_logic;
SIGNAL \nReset_i~combout\ : std_logic;
SIGNAL \MSS|WideOr5~0_combout\ : std_logic;
SIGNAL \top_ms_i~combout\ : std_logic;
SIGNAL \~GND~combout\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[0]~1\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[0]~1COUT1_28\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[1]~3\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[1]~3COUT1_30\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[2]~13\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[2]~13COUT1_32\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[3]~5\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[3]~5COUT1_34\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[4]~7\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[5]~15\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[5]~15COUT1_36\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[6]~17\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[6]~17COUT1_38\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[7]~9\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[7]~9COUT1_40\ : std_logic;
SIGNAL \TIMER_inst|Equal3~0_combout\ : std_logic;
SIGNAL \TIMER_inst|Equal3~1_combout\ : std_logic;
SIGNAL \TIMER_inst|trigger2_o~0_combout\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s[8]~18_combout\ : std_logic;
SIGNAL \TIMER_inst|Equal3~2_combout\ : std_logic;
SIGNAL \MSS|cur_state.TRIG2~regout\ : std_logic;
SIGNAL \TIMER_inst|trigger2_o~2\ : std_logic;
SIGNAL \TIMER_inst|trigger2_o~1\ : std_logic;
SIGNAL \TIMER_inst|trigger2_o\ : std_logic;
SIGNAL \MSS|cur_state.CLIC2_W~regout\ : std_logic;
SIGNAL \MSS|Selector4~0_combout\ : std_logic;
SIGNAL \MSS|cur_state.ERR~regout\ : std_logic;
SIGNAL \MSS|cur_state.DBCLIC~regout\ : std_logic;
SIGNAL \det_s~0_combout\ : std_logic;
SIGNAL \MSS|cur_state.START~regout\ : std_logic;
SIGNAL \MSS|cur_state.TRIG1~regout\ : std_logic;
SIGNAL \MSS|cur_state.CLIC1_W~regout\ : std_logic;
SIGNAL \MSS|cur_state.TRIGW~regout\ : std_logic;
SIGNAL \MSS|cur_state.WAIT_C~regout\ : std_logic;
SIGNAL \MSS|cur_state.CLIC~regout\ : std_logic;
SIGNAL \cs_lg_s~combout\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[0]~22_combout\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[0]~3\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[0]~3COUT1_33\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[1]~5\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[1]~5COUT1_35\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[2]~7\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[2]~7COUT1_37\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[3]~9\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[3]~9COUT1_39\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[4]~11\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[5]~13\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[5]~13COUT1_41\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[6]~15\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[6]~15COUT1_43\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[7]~17\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[7]~17COUT1_45\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[8]~19\ : std_logic;
SIGNAL \MAINTIENC|cnt_pres[8]~19COUT1_47\ : std_logic;
SIGNAL \MAINTIENC|Equal0~1_combout\ : std_logic;
SIGNAL \MAINTIENC|Equal0~0_combout\ : std_logic;
SIGNAL \MAINTIENC|Equal0~2_combout\ : std_logic;
SIGNAL \clic_lg_o~0_combout\ : std_logic;
SIGNAL \dbl_clic_lg_o~0_combout\ : std_logic;
SIGNAL \TIMER_inst|cpt_pres_s\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \MAINTIENC|cnt_pres\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \TIMER_inst|cpt_past_s\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \ALT_INV_nReset_i~combout\ : std_logic;

BEGIN

ww_clock_i <= clock_i;
ww_nReset_i <= nReset_i;
ww_button_i <= button_i;
ww_top_ms_i <= top_ms_i;
clic_o <= ww_clic_o;
dbl_clic_o <= ww_dbl_clic_o;
clic_lg_o <= ww_clic_lg_o;
dbl_clic_lg_o <= ww_dbl_clic_lg_o;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_nReset_i~combout\ <= NOT \nReset_i~combout\;

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clock_i~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_clock_i,
	combout => \clock_i~combout\);

-- Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\button_i~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_button_i,
	combout => \button_i~combout\);

-- Location: PIN_J5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\nReset_i~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_nReset_i,
	combout => \nReset_i~combout\);

-- Location: LC_X13_Y8_N6
\MSS|WideOr5~0\ : maxv_lcell
-- Equation(s):
-- \MSS|WideOr5~0_combout\ = (\MSS|cur_state.TRIGW~regout\) # ((\MSS|cur_state.TRIG2~regout\) # ((\MSS|cur_state.TRIG1~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fefe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MSS|cur_state.TRIGW~regout\,
	datab => \MSS|cur_state.TRIG2~regout\,
	datac => \MSS|cur_state.TRIG1~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MSS|WideOr5~0_combout\);

-- Location: PIN_F11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\top_ms_i~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_top_ms_i,
	combout => \top_ms_i~combout\);

-- Location: LC_X12_Y8_N6
\~GND\ : maxv_lcell
-- Equation(s):
-- \~GND~combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \~GND~combout\);

-- Location: LC_X15_Y8_N0
\TIMER_inst|cpt_pres_s[0]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(0) = DFFEAS(((!\TIMER_inst|cpt_pres_s\(0))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, VCC, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[0]~1\ = CARRY(((\TIMER_inst|cpt_pres_s\(0))))
-- \TIMER_inst|cpt_pres_s[0]~1COUT1_28\ = CARRY(((\TIMER_inst|cpt_pres_s\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \TIMER_inst|cpt_pres_s\(0),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(0),
	cout0 => \TIMER_inst|cpt_pres_s[0]~1\,
	cout1 => \TIMER_inst|cpt_pres_s[0]~1COUT1_28\);

-- Location: LC_X15_Y8_N1
\TIMER_inst|cpt_pres_s[1]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(1) = DFFEAS((\TIMER_inst|cpt_pres_s\(1) $ ((!\TIMER_inst|cpt_pres_s[0]~1\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, \~GND~combout\, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[1]~3\ = CARRY(((!\TIMER_inst|cpt_pres_s\(1) & !\TIMER_inst|cpt_pres_s[0]~1\)))
-- \TIMER_inst|cpt_pres_s[1]~3COUT1_30\ = CARRY(((!\TIMER_inst|cpt_pres_s\(1) & !\TIMER_inst|cpt_pres_s[0]~1COUT1_28\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c303",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \TIMER_inst|cpt_pres_s\(1),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin0 => \TIMER_inst|cpt_pres_s[0]~1\,
	cin1 => \TIMER_inst|cpt_pres_s[0]~1COUT1_28\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(1),
	cout0 => \TIMER_inst|cpt_pres_s[1]~3\,
	cout1 => \TIMER_inst|cpt_pres_s[1]~3COUT1_30\);

-- Location: LC_X15_Y8_N2
\TIMER_inst|cpt_pres_s[2]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(2) = DFFEAS((\TIMER_inst|cpt_pres_s\(2) $ ((\TIMER_inst|cpt_pres_s[1]~3\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, VCC, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[2]~13\ = CARRY(((\TIMER_inst|cpt_pres_s\(2)) # (!\TIMER_inst|cpt_pres_s[1]~3\)))
-- \TIMER_inst|cpt_pres_s[2]~13COUT1_32\ = CARRY(((\TIMER_inst|cpt_pres_s\(2)) # (!\TIMER_inst|cpt_pres_s[1]~3COUT1_30\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3ccf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \TIMER_inst|cpt_pres_s\(2),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin0 => \TIMER_inst|cpt_pres_s[1]~3\,
	cin1 => \TIMER_inst|cpt_pres_s[1]~3COUT1_30\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(2),
	cout0 => \TIMER_inst|cpt_pres_s[2]~13\,
	cout1 => \TIMER_inst|cpt_pres_s[2]~13COUT1_32\);

-- Location: LC_X15_Y8_N3
\TIMER_inst|cpt_pres_s[3]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(3) = DFFEAS(\TIMER_inst|cpt_pres_s\(3) $ ((((!\TIMER_inst|cpt_pres_s[2]~13\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, VCC, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[3]~5\ = CARRY((!\TIMER_inst|cpt_pres_s\(3) & ((!\TIMER_inst|cpt_pres_s[2]~13\))))
-- \TIMER_inst|cpt_pres_s[3]~5COUT1_34\ = CARRY((!\TIMER_inst|cpt_pres_s\(3) & ((!\TIMER_inst|cpt_pres_s[2]~13COUT1_32\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a505",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_pres_s\(3),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin0 => \TIMER_inst|cpt_pres_s[2]~13\,
	cin1 => \TIMER_inst|cpt_pres_s[2]~13COUT1_32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(3),
	cout0 => \TIMER_inst|cpt_pres_s[3]~5\,
	cout1 => \TIMER_inst|cpt_pres_s[3]~5COUT1_34\);

-- Location: LC_X15_Y8_N4
\TIMER_inst|cpt_pres_s[4]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(4) = DFFEAS(\TIMER_inst|cpt_pres_s\(4) $ ((((\TIMER_inst|cpt_pres_s[3]~5\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, \~GND~combout\, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[4]~7\ = CARRY((\TIMER_inst|cpt_pres_s\(4)) # ((!\TIMER_inst|cpt_pres_s[3]~5COUT1_34\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5aaf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_pres_s\(4),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin0 => \TIMER_inst|cpt_pres_s[3]~5\,
	cin1 => \TIMER_inst|cpt_pres_s[3]~5COUT1_34\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(4),
	cout => \TIMER_inst|cpt_pres_s[4]~7\);

-- Location: LC_X15_Y8_N5
\TIMER_inst|cpt_pres_s[5]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(5) = DFFEAS(\TIMER_inst|cpt_pres_s\(5) $ ((((!\TIMER_inst|cpt_pres_s[4]~7\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, VCC, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[5]~15\ = CARRY((!\TIMER_inst|cpt_pres_s\(5) & ((!\TIMER_inst|cpt_pres_s[4]~7\))))
-- \TIMER_inst|cpt_pres_s[5]~15COUT1_36\ = CARRY((!\TIMER_inst|cpt_pres_s\(5) & ((!\TIMER_inst|cpt_pres_s[4]~7\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a505",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_pres_s\(5),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin => \TIMER_inst|cpt_pres_s[4]~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(5),
	cout0 => \TIMER_inst|cpt_pres_s[5]~15\,
	cout1 => \TIMER_inst|cpt_pres_s[5]~15COUT1_36\);

-- Location: LC_X15_Y8_N6
\TIMER_inst|cpt_pres_s[6]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(6) = DFFEAS(\TIMER_inst|cpt_pres_s\(6) $ (((((!\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[5]~15\) # (\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[5]~15COUT1_36\))))), GLOBAL(\clock_i~combout\), 
-- GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, \~GND~combout\, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[6]~17\ = CARRY((\TIMER_inst|cpt_pres_s\(6)) # ((!\TIMER_inst|cpt_pres_s[5]~15\)))
-- \TIMER_inst|cpt_pres_s[6]~17COUT1_38\ = CARRY((\TIMER_inst|cpt_pres_s\(6)) # ((!\TIMER_inst|cpt_pres_s[5]~15COUT1_36\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5aaf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_pres_s\(6),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin => \TIMER_inst|cpt_pres_s[4]~7\,
	cin0 => \TIMER_inst|cpt_pres_s[5]~15\,
	cin1 => \TIMER_inst|cpt_pres_s[5]~15COUT1_36\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(6),
	cout0 => \TIMER_inst|cpt_pres_s[6]~17\,
	cout1 => \TIMER_inst|cpt_pres_s[6]~17COUT1_38\);

-- Location: LC_X15_Y8_N7
\TIMER_inst|cpt_pres_s[7]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(7) = DFFEAS((\TIMER_inst|cpt_pres_s\(7) $ ((!(!\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[6]~17\) # (\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[6]~17COUT1_38\)))), GLOBAL(\clock_i~combout\), 
-- GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, \~GND~combout\, , , \MSS|WideOr5~0_combout\)
-- \TIMER_inst|cpt_pres_s[7]~9\ = CARRY(((!\TIMER_inst|cpt_pres_s\(7) & !\TIMER_inst|cpt_pres_s[6]~17\)))
-- \TIMER_inst|cpt_pres_s[7]~9COUT1_40\ = CARRY(((!\TIMER_inst|cpt_pres_s\(7) & !\TIMER_inst|cpt_pres_s[6]~17COUT1_38\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c303",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \TIMER_inst|cpt_pres_s\(7),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin => \TIMER_inst|cpt_pres_s[4]~7\,
	cin0 => \TIMER_inst|cpt_pres_s[6]~17\,
	cin1 => \TIMER_inst|cpt_pres_s[6]~17COUT1_38\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(7),
	cout0 => \TIMER_inst|cpt_pres_s[7]~9\,
	cout1 => \TIMER_inst|cpt_pres_s[7]~9COUT1_40\);

-- Location: LC_X15_Y8_N8
\TIMER_inst|cpt_pres_s[8]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s\(8) = DFFEAS((((!\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[7]~9\) # (\TIMER_inst|cpt_pres_s[4]~7\ & \TIMER_inst|cpt_pres_s[7]~9COUT1_40\) $ (\TIMER_inst|cpt_pres_s\(8)))), GLOBAL(\clock_i~combout\), 
-- GLOBAL(\nReset_i~combout\), , \TIMER_inst|cpt_pres_s[8]~18_combout\, VCC, , , \MSS|WideOr5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datac => VCC,
	datad => \TIMER_inst|cpt_pres_s\(8),
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \MSS|WideOr5~0_combout\,
	ena => \TIMER_inst|cpt_pres_s[8]~18_combout\,
	cin => \TIMER_inst|cpt_pres_s[4]~7\,
	cin0 => \TIMER_inst|cpt_pres_s[7]~9\,
	cin1 => \TIMER_inst|cpt_pres_s[7]~9COUT1_40\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_pres_s\(8));

-- Location: LC_X14_Y8_N1
\TIMER_inst|Equal3~0\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|Equal3~0_combout\ = (!\TIMER_inst|cpt_pres_s\(3) & (!\TIMER_inst|cpt_pres_s\(4) & (!\TIMER_inst|cpt_pres_s\(0) & !\TIMER_inst|cpt_pres_s\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER_inst|cpt_pres_s\(3),
	datab => \TIMER_inst|cpt_pres_s\(4),
	datac => \TIMER_inst|cpt_pres_s\(0),
	datad => \TIMER_inst|cpt_pres_s\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|Equal3~0_combout\);

-- Location: LC_X14_Y8_N2
\TIMER_inst|Equal3~1\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|Equal3~1_combout\ = (!\TIMER_inst|cpt_pres_s\(7) & (!\TIMER_inst|cpt_pres_s\(8) & ((\TIMER_inst|Equal3~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER_inst|cpt_pres_s\(7),
	datab => \TIMER_inst|cpt_pres_s\(8),
	datad => \TIMER_inst|Equal3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|Equal3~1_combout\);

-- Location: LC_X14_Y8_N3
\TIMER_inst|trigger2_o~0\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|trigger2_o~0_combout\ = (!\TIMER_inst|cpt_pres_s\(6) & (!\TIMER_inst|cpt_pres_s\(5) & (!\TIMER_inst|cpt_pres_s\(2) & \TIMER_inst|Equal3~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER_inst|cpt_pres_s\(6),
	datab => \TIMER_inst|cpt_pres_s\(5),
	datac => \TIMER_inst|cpt_pres_s\(2),
	datad => \TIMER_inst|Equal3~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|trigger2_o~0_combout\);

-- Location: LC_X15_Y8_N9
\TIMER_inst|cpt_pres_s[8]~18\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_pres_s[8]~18_combout\ = ((\MSS|WideOr5~0_combout\) # ((\top_ms_i~combout\ & !\TIMER_inst|trigger2_o~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0fc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \top_ms_i~combout\,
	datac => \MSS|WideOr5~0_combout\,
	datad => \TIMER_inst|trigger2_o~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|cpt_pres_s[8]~18_combout\);

-- Location: LC_X13_Y8_N8
\TIMER_inst|Equal3~2\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|Equal3~2_combout\ = (\TIMER_inst|cpt_pres_s\(5) & (\TIMER_inst|cpt_pres_s\(2) & (\TIMER_inst|Equal3~1_combout\ & \TIMER_inst|cpt_pres_s\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \TIMER_inst|cpt_pres_s\(5),
	datab => \TIMER_inst|cpt_pres_s\(2),
	datac => \TIMER_inst|Equal3~1_combout\,
	datad => \TIMER_inst|cpt_pres_s\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|Equal3~2_combout\);

-- Location: LC_X13_Y8_N9
\MSS|cur_state.TRIG2\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.TRIG2~regout\ = DFFEAS(((\MSS|cur_state.WAIT_C~regout\ & (\button_i~combout\ & !\TIMER_inst|Equal3~2_combout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00c0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MSS|cur_state.WAIT_C~regout\,
	datac => \button_i~combout\,
	datad => \TIMER_inst|Equal3~2_combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.TRIG2~regout\);

-- Location: LC_X13_Y8_N4
\TIMER_inst|cpt_past_s[2]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(2) = DFFEAS((((\TIMER_inst|cpt_pres_s\(2)))), GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datad => \TIMER_inst|cpt_pres_s\(2),
	aclr => GND,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(2));

-- Location: LC_X13_Y8_N7
\TIMER_inst|cpt_past_s[1]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(1) = DFFEAS(GND, GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, \TIMER_inst|cpt_pres_s\(1), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datac => \TIMER_inst|cpt_pres_s\(1),
	aclr => GND,
	sload => VCC,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(1));

-- Location: LC_X13_Y8_N3
\TIMER_inst|cpt_past_s[3]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(3) = DFFEAS(GND, GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, \TIMER_inst|cpt_pres_s\(3), , , VCC)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datac => \TIMER_inst|cpt_pres_s\(3),
	aclr => GND,
	sload => VCC,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(3));

-- Location: LC_X13_Y8_N2
\TIMER_inst|cpt_past_s[4]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|trigger2_o~2\ = (!\TIMER_inst|cpt_past_s\(2) & (!\TIMER_inst|cpt_past_s\(1) & (!D1_cpt_past_s[4] & !\TIMER_inst|cpt_past_s\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "qfbk",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_past_s\(2),
	datab => \TIMER_inst|cpt_past_s\(1),
	datac => \TIMER_inst|cpt_pres_s\(4),
	datad => \TIMER_inst|cpt_past_s\(3),
	aclr => GND,
	sload => VCC,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|trigger2_o~2\,
	regout => \TIMER_inst|cpt_past_s\(4));

-- Location: LC_X14_Y7_N3
\TIMER_inst|cpt_past_s[7]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(7) = DFFEAS((((\TIMER_inst|cpt_pres_s\(7)))), GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datad => \TIMER_inst|cpt_pres_s\(7),
	aclr => GND,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(7));

-- Location: LC_X14_Y7_N9
\TIMER_inst|cpt_past_s[6]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(6) = DFFEAS((((\TIMER_inst|cpt_pres_s\(6)))), GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datad => \TIMER_inst|cpt_pres_s\(6),
	aclr => GND,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(6));

-- Location: LC_X14_Y7_N2
\TIMER_inst|cpt_past_s[5]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|cpt_past_s\(5) = DFFEAS((((\TIMER_inst|cpt_pres_s\(5)))), GLOBAL(\clock_i~combout\), VCC, , \nReset_i~combout\, , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datad => \TIMER_inst|cpt_pres_s\(5),
	aclr => GND,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \TIMER_inst|cpt_past_s\(5));

-- Location: LC_X14_Y7_N5
\TIMER_inst|cpt_past_s[8]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|trigger2_o~1\ = (!\TIMER_inst|cpt_past_s\(7) & (!\TIMER_inst|cpt_past_s\(6) & (!D1_cpt_past_s[8] & !\TIMER_inst|cpt_past_s\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "qfbk",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|cpt_past_s\(7),
	datab => \TIMER_inst|cpt_past_s\(6),
	datac => \TIMER_inst|cpt_pres_s\(8),
	datad => \TIMER_inst|cpt_past_s\(5),
	aclr => GND,
	sload => VCC,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|trigger2_o~1\,
	regout => \TIMER_inst|cpt_past_s\(8));

-- Location: LC_X14_Y8_N0
\TIMER_inst|cpt_past_s[0]\ : maxv_lcell
-- Equation(s):
-- \TIMER_inst|trigger2_o\ = (\TIMER_inst|trigger2_o~0_combout\ & (\TIMER_inst|trigger2_o~2\ & (D1_cpt_past_s[0] & \TIMER_inst|trigger2_o~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "qfbk",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \TIMER_inst|trigger2_o~0_combout\,
	datab => \TIMER_inst|trigger2_o~2\,
	datac => \TIMER_inst|cpt_pres_s\(0),
	datad => \TIMER_inst|trigger2_o~1\,
	aclr => GND,
	sload => VCC,
	ena => \nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \TIMER_inst|trigger2_o\,
	regout => \TIMER_inst|cpt_past_s\(0));

-- Location: LC_X14_Y8_N9
\MSS|cur_state.CLIC2_W\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.CLIC2_W~regout\ = DFFEAS((\MSS|cur_state.TRIG2~regout\) # ((\MSS|cur_state.CLIC2_W~regout\ & (!\TIMER_inst|trigger2_o\ & \button_i~combout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "aeaa",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.TRIG2~regout\,
	datab => \MSS|cur_state.CLIC2_W~regout\,
	datac => \TIMER_inst|trigger2_o\,
	datad => \button_i~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.CLIC2_W~regout\);

-- Location: LC_X14_Y8_N8
\MSS|Selector4~0\ : maxv_lcell
-- Equation(s):
-- \MSS|Selector4~0_combout\ = ((\MSS|cur_state.CLIC2_W~regout\) # ((\MSS|cur_state.CLIC1_W~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fcfc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \MSS|cur_state.CLIC2_W~regout\,
	datac => \MSS|cur_state.CLIC1_W~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MSS|Selector4~0_combout\);

-- Location: LC_X14_Y8_N7
\MSS|cur_state.ERR\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.ERR~regout\ = DFFEAS((\MSS|Selector4~0_combout\ & ((\TIMER_inst|trigger2_o\) # ((\MSS|cur_state.ERR~regout\ & \button_i~combout\)))) # (!\MSS|Selector4~0_combout\ & (\MSS|cur_state.ERR~regout\ & ((\button_i~combout\)))), 
-- GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "eca0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|Selector4~0_combout\,
	datab => \MSS|cur_state.ERR~regout\,
	datac => \TIMER_inst|trigger2_o\,
	datad => \button_i~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.ERR~regout\);

-- Location: LC_X14_Y8_N5
\MSS|cur_state.DBCLIC\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.DBCLIC~regout\ = DFFEAS(((\MSS|cur_state.CLIC2_W~regout\ & (!\TIMER_inst|trigger2_o\ & !\button_i~combout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000c",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MSS|cur_state.CLIC2_W~regout\,
	datac => \TIMER_inst|trigger2_o\,
	datad => \button_i~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.DBCLIC~regout\);

-- Location: LC_X12_Y8_N7
\det_s~0\ : maxv_lcell
-- Equation(s):
-- \det_s~0_combout\ = (((\MSS|cur_state.DBCLIC~regout\) # (\MSS|cur_state.CLIC~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fff0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \MSS|cur_state.DBCLIC~regout\,
	datad => \MSS|cur_state.CLIC~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \det_s~0_combout\);

-- Location: LC_X13_Y8_N0
\MSS|cur_state.START\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.START~regout\ = DFFEAS((!\det_s~0_combout\ & ((\button_i~combout\) # ((!\MSS|cur_state.ERR~regout\ & \MSS|cur_state.START~regout\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00f4",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.ERR~regout\,
	datab => \MSS|cur_state.START~regout\,
	datac => \button_i~combout\,
	datad => \det_s~0_combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.START~regout\);

-- Location: LC_X13_Y8_N5
\MSS|cur_state.TRIG1\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.TRIG1~regout\ = DFFEAS(((\button_i~combout\ & (!\MSS|cur_state.START~regout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0c0c",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \button_i~combout\,
	datac => \MSS|cur_state.START~regout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.TRIG1~regout\);

-- Location: LC_X14_Y8_N4
\MSS|cur_state.CLIC1_W\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.CLIC1_W~regout\ = DFFEAS((\MSS|cur_state.TRIG1~regout\) # ((\button_i~combout\ & (!\TIMER_inst|trigger2_o\ & \MSS|cur_state.CLIC1_W~regout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "aeaa",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.TRIG1~regout\,
	datab => \button_i~combout\,
	datac => \TIMER_inst|trigger2_o\,
	datad => \MSS|cur_state.CLIC1_W~regout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.CLIC1_W~regout\);

-- Location: LC_X14_Y8_N6
\MSS|cur_state.TRIGW\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.TRIGW~regout\ = DFFEAS((\MSS|cur_state.CLIC1_W~regout\ & (!\button_i~combout\ & (!\TIMER_inst|trigger2_o\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0202",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.CLIC1_W~regout\,
	datab => \button_i~combout\,
	datac => \TIMER_inst|trigger2_o\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.TRIGW~regout\);

-- Location: LC_X13_Y8_N1
\MSS|cur_state.WAIT_C\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.WAIT_C~regout\ = DFFEAS((\MSS|cur_state.TRIGW~regout\) # ((\MSS|cur_state.WAIT_C~regout\ & (!\button_i~combout\ & !\TIMER_inst|Equal3~2_combout\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "aaae",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.TRIGW~regout\,
	datab => \MSS|cur_state.WAIT_C~regout\,
	datac => \button_i~combout\,
	datad => \TIMER_inst|Equal3~2_combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.WAIT_C~regout\);

-- Location: LC_X12_Y8_N9
\MSS|cur_state.CLIC\ : maxv_lcell
-- Equation(s):
-- \MSS|cur_state.CLIC~regout\ = DFFEAS((\MSS|cur_state.WAIT_C~regout\ & (((\TIMER_inst|Equal3~2_combout\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "aa00",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MSS|cur_state.WAIT_C~regout\,
	datad => \TIMER_inst|Equal3~2_combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MSS|cur_state.CLIC~regout\);

-- Location: LC_X12_Y8_N8
cs_lg_s : maxv_lcell
-- Equation(s):
-- \cs_lg_s~combout\ = ((!\MSS|cur_state.DBCLIC~regout\ & ((\MSS|cur_state.CLIC~regout\) # (\cs_lg_s~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0f0c",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \MSS|cur_state.CLIC~regout\,
	datac => \MSS|cur_state.DBCLIC~regout\,
	datad => \cs_lg_s~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \cs_lg_s~combout\);

-- Location: LC_X12_Y8_N2
\MAINTIENC|cnt_pres[0]~22\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres[0]~22_combout\ = (\MSS|cur_state.CLIC~regout\) # ((\MSS|cur_state.DBCLIC~regout\) # ((\top_ms_i~combout\ & \MAINTIENC|Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fefc",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \top_ms_i~combout\,
	datab => \MSS|cur_state.CLIC~regout\,
	datac => \MSS|cur_state.DBCLIC~regout\,
	datad => \MAINTIENC|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MAINTIENC|cnt_pres[0]~22_combout\);

-- Location: LC_X11_Y8_N0
\MAINTIENC|cnt_pres[0]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(0) = DFFEAS(((!\MAINTIENC|cnt_pres\(0))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[0]~3\ = CARRY(((\MAINTIENC|cnt_pres\(0))))
-- \MAINTIENC|cnt_pres[0]~3COUT1_33\ = CARRY(((\MAINTIENC|cnt_pres\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MAINTIENC|cnt_pres\(0),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(0),
	cout0 => \MAINTIENC|cnt_pres[0]~3\,
	cout1 => \MAINTIENC|cnt_pres[0]~3COUT1_33\);

-- Location: LC_X11_Y8_N1
\MAINTIENC|cnt_pres[1]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(1) = DFFEAS((\MAINTIENC|cnt_pres\(1) $ ((!\MAINTIENC|cnt_pres[0]~3\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, \~GND~combout\, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[1]~5\ = CARRY(((!\MAINTIENC|cnt_pres\(1) & !\MAINTIENC|cnt_pres[0]~3\)))
-- \MAINTIENC|cnt_pres[1]~5COUT1_35\ = CARRY(((!\MAINTIENC|cnt_pres\(1) & !\MAINTIENC|cnt_pres[0]~3COUT1_33\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c303",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MAINTIENC|cnt_pres\(1),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin0 => \MAINTIENC|cnt_pres[0]~3\,
	cin1 => \MAINTIENC|cnt_pres[0]~3COUT1_33\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(1),
	cout0 => \MAINTIENC|cnt_pres[1]~5\,
	cout1 => \MAINTIENC|cnt_pres[1]~5COUT1_35\);

-- Location: LC_X11_Y8_N2
\MAINTIENC|cnt_pres[2]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(2) = DFFEAS((\MAINTIENC|cnt_pres\(2) $ ((\MAINTIENC|cnt_pres[1]~5\))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[2]~7\ = CARRY(((\MAINTIENC|cnt_pres\(2)) # (!\MAINTIENC|cnt_pres[1]~5\)))
-- \MAINTIENC|cnt_pres[2]~7COUT1_37\ = CARRY(((\MAINTIENC|cnt_pres\(2)) # (!\MAINTIENC|cnt_pres[1]~5COUT1_35\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3ccf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MAINTIENC|cnt_pres\(2),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin0 => \MAINTIENC|cnt_pres[1]~5\,
	cin1 => \MAINTIENC|cnt_pres[1]~5COUT1_35\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(2),
	cout0 => \MAINTIENC|cnt_pres[2]~7\,
	cout1 => \MAINTIENC|cnt_pres[2]~7COUT1_37\);

-- Location: LC_X11_Y8_N3
\MAINTIENC|cnt_pres[3]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(3) = DFFEAS(\MAINTIENC|cnt_pres\(3) $ ((((!\MAINTIENC|cnt_pres[2]~7\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, \~GND~combout\, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[3]~9\ = CARRY((!\MAINTIENC|cnt_pres\(3) & ((!\MAINTIENC|cnt_pres[2]~7\))))
-- \MAINTIENC|cnt_pres[3]~9COUT1_39\ = CARRY((!\MAINTIENC|cnt_pres\(3) & ((!\MAINTIENC|cnt_pres[2]~7COUT1_37\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a505",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MAINTIENC|cnt_pres\(3),
	datac => \~GND~combout\,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin0 => \MAINTIENC|cnt_pres[2]~7\,
	cin1 => \MAINTIENC|cnt_pres[2]~7COUT1_37\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(3),
	cout0 => \MAINTIENC|cnt_pres[3]~9\,
	cout1 => \MAINTIENC|cnt_pres[3]~9COUT1_39\);

-- Location: LC_X11_Y8_N4
\MAINTIENC|cnt_pres[4]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(4) = DFFEAS(\MAINTIENC|cnt_pres\(4) $ ((((\MAINTIENC|cnt_pres[3]~9\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[4]~11\ = CARRY((\MAINTIENC|cnt_pres\(4)) # ((!\MAINTIENC|cnt_pres[3]~9COUT1_39\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5aaf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MAINTIENC|cnt_pres\(4),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin0 => \MAINTIENC|cnt_pres[3]~9\,
	cin1 => \MAINTIENC|cnt_pres[3]~9COUT1_39\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(4),
	cout => \MAINTIENC|cnt_pres[4]~11\);

-- Location: LC_X11_Y8_N5
\MAINTIENC|cnt_pres[5]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(5) = DFFEAS(\MAINTIENC|cnt_pres\(5) $ ((((!\MAINTIENC|cnt_pres[4]~11\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[5]~13\ = CARRY((!\MAINTIENC|cnt_pres\(5) & ((!\MAINTIENC|cnt_pres[4]~11\))))
-- \MAINTIENC|cnt_pres[5]~13COUT1_41\ = CARRY((!\MAINTIENC|cnt_pres\(5) & ((!\MAINTIENC|cnt_pres[4]~11\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a505",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MAINTIENC|cnt_pres\(5),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin => \MAINTIENC|cnt_pres[4]~11\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(5),
	cout0 => \MAINTIENC|cnt_pres[5]~13\,
	cout1 => \MAINTIENC|cnt_pres[5]~13COUT1_41\);

-- Location: LC_X11_Y8_N6
\MAINTIENC|cnt_pres[6]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(6) = DFFEAS(\MAINTIENC|cnt_pres\(6) $ (((((!\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[5]~13\) # (\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[5]~13COUT1_41\))))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , 
-- \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[6]~15\ = CARRY((\MAINTIENC|cnt_pres\(6)) # ((!\MAINTIENC|cnt_pres[5]~13\)))
-- \MAINTIENC|cnt_pres[6]~15COUT1_43\ = CARRY((\MAINTIENC|cnt_pres\(6)) # ((!\MAINTIENC|cnt_pres[5]~13COUT1_41\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5aaf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MAINTIENC|cnt_pres\(6),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin => \MAINTIENC|cnt_pres[4]~11\,
	cin0 => \MAINTIENC|cnt_pres[5]~13\,
	cin1 => \MAINTIENC|cnt_pres[5]~13COUT1_41\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(6),
	cout0 => \MAINTIENC|cnt_pres[6]~15\,
	cout1 => \MAINTIENC|cnt_pres[6]~15COUT1_43\);

-- Location: LC_X11_Y8_N7
\MAINTIENC|cnt_pres[7]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(7) = DFFEAS((\MAINTIENC|cnt_pres\(7) $ ((!(!\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[6]~15\) # (\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[6]~15COUT1_43\)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , 
-- \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[7]~17\ = CARRY(((!\MAINTIENC|cnt_pres\(7) & !\MAINTIENC|cnt_pres[6]~15\)))
-- \MAINTIENC|cnt_pres[7]~17COUT1_45\ = CARRY(((!\MAINTIENC|cnt_pres\(7) & !\MAINTIENC|cnt_pres[6]~15COUT1_43\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c303",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datab => \MAINTIENC|cnt_pres\(7),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin => \MAINTIENC|cnt_pres[4]~11\,
	cin0 => \MAINTIENC|cnt_pres[6]~15\,
	cin1 => \MAINTIENC|cnt_pres[6]~15COUT1_43\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(7),
	cout0 => \MAINTIENC|cnt_pres[7]~17\,
	cout1 => \MAINTIENC|cnt_pres[7]~17COUT1_45\);

-- Location: LC_X11_Y8_N8
\MAINTIENC|cnt_pres[8]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(8) = DFFEAS(\MAINTIENC|cnt_pres\(8) $ (((((!\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[7]~17\) # (\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[7]~17COUT1_45\))))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , 
-- \MAINTIENC|cnt_pres[0]~22_combout\, VCC, , , \det_s~0_combout\)
-- \MAINTIENC|cnt_pres[8]~19\ = CARRY((\MAINTIENC|cnt_pres\(8)) # ((!\MAINTIENC|cnt_pres[7]~17\)))
-- \MAINTIENC|cnt_pres[8]~19COUT1_47\ = CARRY((\MAINTIENC|cnt_pres\(8)) # ((!\MAINTIENC|cnt_pres[7]~17COUT1_45\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5aaf",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	dataa => \MAINTIENC|cnt_pres\(8),
	datac => VCC,
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin => \MAINTIENC|cnt_pres[4]~11\,
	cin0 => \MAINTIENC|cnt_pres[7]~17\,
	cin1 => \MAINTIENC|cnt_pres[7]~17COUT1_45\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(8),
	cout0 => \MAINTIENC|cnt_pres[8]~19\,
	cout1 => \MAINTIENC|cnt_pres[8]~19COUT1_47\);

-- Location: LC_X11_Y8_N9
\MAINTIENC|cnt_pres[9]\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|cnt_pres\(9) = DFFEAS((((!\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[8]~19\) # (\MAINTIENC|cnt_pres[4]~11\ & \MAINTIENC|cnt_pres[8]~19COUT1_47\) $ (!\MAINTIENC|cnt_pres\(9)))), GLOBAL(\clock_i~combout\), GLOBAL(\nReset_i~combout\), , 
-- \MAINTIENC|cnt_pres[0]~22_combout\, \~GND~combout\, , , \det_s~0_combout\)

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \clock_i~combout\,
	datac => \~GND~combout\,
	datad => \MAINTIENC|cnt_pres\(9),
	aclr => \ALT_INV_nReset_i~combout\,
	sload => \det_s~0_combout\,
	ena => \MAINTIENC|cnt_pres[0]~22_combout\,
	cin => \MAINTIENC|cnt_pres[4]~11\,
	cin0 => \MAINTIENC|cnt_pres[8]~19\,
	cin1 => \MAINTIENC|cnt_pres[8]~19COUT1_47\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \MAINTIENC|cnt_pres\(9));

-- Location: LC_X12_Y8_N4
\MAINTIENC|Equal0~1\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|Equal0~1_combout\ = (\MAINTIENC|cnt_pres\(5)) # ((\MAINTIENC|cnt_pres\(4)) # ((\MAINTIENC|cnt_pres\(6)) # (\MAINTIENC|cnt_pres\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MAINTIENC|cnt_pres\(5),
	datab => \MAINTIENC|cnt_pres\(4),
	datac => \MAINTIENC|cnt_pres\(6),
	datad => \MAINTIENC|cnt_pres\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MAINTIENC|Equal0~1_combout\);

-- Location: LC_X12_Y8_N0
\MAINTIENC|Equal0~0\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|Equal0~0_combout\ = (\MAINTIENC|cnt_pres\(0)) # ((\MAINTIENC|cnt_pres\(2)) # ((\MAINTIENC|cnt_pres\(1)) # (\MAINTIENC|cnt_pres\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MAINTIENC|cnt_pres\(0),
	datab => \MAINTIENC|cnt_pres\(2),
	datac => \MAINTIENC|cnt_pres\(1),
	datad => \MAINTIENC|cnt_pres\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MAINTIENC|Equal0~0_combout\);

-- Location: LC_X12_Y8_N1
\MAINTIENC|Equal0~2\ : maxv_lcell
-- Equation(s):
-- \MAINTIENC|Equal0~2_combout\ = (\MAINTIENC|cnt_pres\(9)) # ((\MAINTIENC|cnt_pres\(8)) # ((\MAINTIENC|Equal0~1_combout\) # (\MAINTIENC|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fffe",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \MAINTIENC|cnt_pres\(9),
	datab => \MAINTIENC|cnt_pres\(8),
	datac => \MAINTIENC|Equal0~1_combout\,
	datad => \MAINTIENC|Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \MAINTIENC|Equal0~2_combout\);

-- Location: LC_X12_Y8_N3
\clic_lg_o~0\ : maxv_lcell
-- Equation(s):
-- \clic_lg_o~0_combout\ = (\cs_lg_s~combout\ & (((\MAINTIENC|Equal0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "aa00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \cs_lg_s~combout\,
	datad => \MAINTIENC|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clic_lg_o~0_combout\);

-- Location: LC_X12_Y8_N5
\dbl_clic_lg_o~0\ : maxv_lcell
-- Equation(s):
-- \dbl_clic_lg_o~0_combout\ = (!\cs_lg_s~combout\ & (((\MAINTIENC|Equal0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \cs_lg_s~combout\,
	datad => \MAINTIENC|Equal0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \dbl_clic_lg_o~0_combout\);

-- Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\clic_o~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \MSS|cur_state.CLIC~regout\,
	oe => VCC,
	padio => ww_clic_o);

-- Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\dbl_clic_o~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \MSS|cur_state.DBCLIC~regout\,
	oe => VCC,
	padio => ww_dbl_clic_o);

-- Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\clic_lg_o~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \clic_lg_o~0_combout\,
	oe => VCC,
	padio => ww_clic_lg_o);

-- Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\dbl_clic_lg_o~I\ : maxv_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \dbl_clic_lg_o~0_combout\,
	oe => VCC,
	padio => ww_dbl_clic_lg_o);
END structure;


